package com.devcamp.s50.car.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "car_type")
public class CCarType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "car_id")
    private CCar car;

   
    @Column(name = "type_code",unique = true)
    private String typeCode;

    public CCarType(String typeCode) {
        this.typeCode = typeCode;
    }
    @Column(name = "type_name")
    private String typeName;

    public CCarType(int id, CCar car, String typeCode, String typeName) {
        this.id = id;
        this.car = car;
        this.typeCode = typeCode;
        this.typeName = typeName;
    }
    public CCarType() {
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
    public CCarType(int id, String typeCode, String typeName) {
        this.id = id;
        this.typeCode = typeCode;
        this.typeName = typeName;
    }
    public int getId() {
        return id;
    }
    public String getTypeCode() {
        return typeCode;
    }
    public String getTypeName() {
        return typeName;
    }
    
}
