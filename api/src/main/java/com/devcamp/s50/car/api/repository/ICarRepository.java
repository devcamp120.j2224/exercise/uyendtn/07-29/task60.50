package com.devcamp.s50.car.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.car.api.model.CCar;

public interface ICarRepository extends JpaRepository<CCar,Long> {

    CCar findBycarCode(String carCode);
    
}
