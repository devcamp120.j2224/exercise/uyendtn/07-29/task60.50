package com.devcamp.s50.car.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.car.api.model.CCarType;

public interface ICarTypeRepository extends JpaRepository<CCarType,Long> {
    
}
